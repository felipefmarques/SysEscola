<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aluno;
use App\Disciplina;
use App\Nota;
use Illuminate\Support\Facades\DB;

class NotasController extends Controller
{

  
    public function index()
    {
        //$notas = Nota::all();
        $notas = DB::table('notas')
            ->select(
                'notas.id_disciplina',
                'notas.id',
                'notas.id_aluno',
                'notas.nota',
                'disciplinas.nome as d_nome',
                'alunos.nome as a_nome'
            )
            ->join(
                'disciplinas',
                'disciplinas.id','=','notas.id_disciplina'
            )
            ->join(
                'alunos',
                'alunos.id','=','notas.id_aluno'
            )
            ->orderBy('a_nome', 'asc')

            ->get();



        return view('lista_notas', compact('notas'));
    }

  
    public function create()
    {
        $alunos = Aluno::all();
        $disciplinas = Disciplina::all();
        return view('add_notas',compact('alunos','disciplinas'));
    }

    public function store(Request $request)
    {
        

        $request->validate([

            'nota' => 'required',
            'id_aluno' => 'required',
            'id_disciplina' => 'required'


        ],[

            'required' => 'O campo :attribute é obrigatorio.',
            
           
        ]);


       $nota = new nota();
       $nota->nota = $request->input('nota');
       $nota->id_aluno = $request->input('id_aluno');
       $nota->id_disciplina = $request->input('id_disciplina');
       $nota->save();

        return redirect('/notas');
    }
  
    public function show($id)
    {
        //
    }
    
    public function edit($id)
    {
        $nota = Nota::find($id);
        $alunos = Aluno::all();
        $disciplinas = Disciplina::all();
        
        if(isset($nota)){

            return view('editar_nota',compact('alunos','disciplinas','nota'));


        }
        return redirect('/notas');
    }
    
    public function update(Request $request, $id)
    {
        $update_nota = Nota::find($id);
        if(isset($update_nota)){

        $update_nota->nota = $request->input('nota');
        $update_nota->id_aluno = $request->input('id_aluno');
        $update_nota->id_disciplina = $request->input('id_disciplina');
        $update_nota->save();

        }
        return redirect('/notas');
    }
   
    public function destroy($id)
    {
        $delete_nota = Nota::find($id);

        if(isset($delete_nota)){

            $delete_nota->delete();

        }

        return redirect('/notas');
    }

    public function relatorio(){


        $notas = DB::table('notas')
            ->select(
                'notas.id_disciplina',
                'notas.id',
                'notas.id_aluno',
                'notas.nota',
                'disciplinas.nome as d_nome',
                'alunos.nome as a_nome'
            )
            ->join(
                'disciplinas',
                'disciplinas.id','=','notas.id_disciplina'
            )
            ->join(
                'alunos',
                'alunos.id','=','notas.id_aluno'
            )
            ->get();

         $medias = array();   
         $media = 0;   
         $total = 0;   
      
        foreach( $notas as $nota){

            if(!array_key_exists($nota->d_nome, $medias)){
                $medias[$nota->d_nome] = array();
            }
            

            if(!array_key_exists($nota->a_nome, $medias[$nota->d_nome])){

                $medias[$nota->d_nome][$nota->a_nome] = array();
                $medias[$nota->d_nome][$nota->a_nome]['total'] = 0;
                $medias[$nota->d_nome][$nota->a_nome]['notas'] = 0;
            }
            
          $total = $medias[$nota->d_nome][$nota->a_nome]['total'];  
          $notas = $medias[$nota->d_nome][$nota->a_nome]['notas'];
          
        $total++;
        $media = $notas + $nota->nota; 

        $medias[$nota->d_nome][$nota->a_nome]['total'] = $total; 
        $medias[$nota->d_nome][$nota->a_nome]['notas'] = $media; 

        } 
       

        foreach ($medias as $d => $a) {
            
            foreach ($a as $key => $obj) {
                
               $medias[$d][$key]['media'] = $obj['notas'] / $obj['total'];
            }
        }


         return view('relatorio',compact('medias'));
    }
}
