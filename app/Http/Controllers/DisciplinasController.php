<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Disciplina;

class DisciplinasController extends Controller
{

 

    public function index()
    {
        $disciplinas = Disciplina::all();
        return view('lista_disciplinas', compact('disciplinas'));
    }

  
    public function create()
    {
        return view('add_disciplina');
    }

    public function store(Request $request)
    {
        

        $request->validate(['nome' => 'required'],[

            'required' => 'O campo :attribute é obrigatorio.'
           
        ]);


       $disciplina = new Disciplina();
       $disciplina->nome = $request->input('nome');
       $disciplina->save();

        return redirect('/disciplinas');
    }
  
    public function show($id)
    {
        //
    }
    
    public function edit($id)
    {
        $disciplina = Disciplina::find($id);
        if(isset($disciplina)){

            return view('editar_disciplina',compact('disciplina'));

        }
        return redirect('/disciplinas');
    }
    
    public function update(Request $request, $id)
    {
        $update_disciplina = Disciplina::find($id);
        if(isset($update_disciplina)){

        $update_disciplina->nome = $request->input('nome');
        $update_disciplina->save();

        }
        return redirect('/disciplinas');
    }
   
    public function destroy($id)
    {
        $delete_disciplina = Disciplina::find($id);

        if(isset($delete_disciplina)){

            $delete_disciplina->delete();

        }

        return redirect('/disciplinas');
    }
}
