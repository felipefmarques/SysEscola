<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aluno;

class AlunosController extends Controller
{

    public function index()
    {
       
       $alunos = Aluno::all();
        return view('lista_alunos', compact('alunos'));


    }


    public function create()
    {

        return view('add_alunos');
    }

    public function store(Request $request)
    {

        $regras = [

            'matricula' => 'required',
            'nome' => 'required',
            'email' => 'required|email',
            'uf' => 'required',
            'cidade' => 'required',
            'cep' => 'required'
        ];

        $mensagens = [

            'required' => 'O campo :attribute é obrigatorio.',
            'email.email' => 'O campo E-mail deve conter um e-mail válido.'
           
        ];

        $request->validate($regras,$mensagens);


       $aluno = new Aluno();
       $aluno->matricula = $request->input('matricula');
       $aluno->nome = $request->input('nome');
       $aluno->email = $request->input('email');
       $aluno->endereco = $request->input('endereco');
       $aluno->bairro = $request->input('bairro');
       $aluno->cidade = $request->input('cidade');
       $aluno->uf = $request->input('uf');
       $aluno->cep = $request->input('cep');
       $aluno->save();

        return redirect('/alunos');

    }


    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $aluno = Aluno::find($id);
        if(isset($aluno)){

            return view('editar_alunos',compact('aluno'));

        }
        return redirect('/alunos');

        
    }

    public function update(Request $request, $id)
    {
        $update_aluno = Aluno::find($id);
        if(isset($update_aluno)){

        $update_aluno->matricula = $request->input('matricula');
        $update_aluno->nome = $request->input('nome');
        $update_aluno->email = $request->input('email');
        $update_aluno->endereco = $request->input('endereco');
        $update_aluno->bairro = $request->input('bairro');
        $update_aluno->cidade = $request->input('cidade');
        $update_aluno->uf = $request->input('uf');
        $update_aluno->cep = $request->input('cep');
        $update_aluno->save();

        }
        return redirect('/alunos');
    }

    public function destroy($id)
    {
        $delete_aluno = Aluno::find($id);

        if(isset($delete_aluno)){

            $delete_aluno->delete();

        }

        return redirect('/alunos');
    }
}
