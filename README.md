<h1><strong>SysEscola  - 1.0</strong></h1>
<p>Sistema para cálculo de médias de notas.</p>
<ul>
	<strong>Requisitos:</strong>
	<li>PHP 7.2 ou superior</li>
	<li>Laravel 5.6 ou superior</li>
	<li>Servidor MySql</li>
</ul>
<h3>Instalação</h3>
<ul>
	<strong>Commands Composer:</strong>
	<li><i>composer install</i></li>
</ul>
<br>
<h2><strong>Banco de dados<strong></h2>
<p>Criar um banco de dados com um nome de sua preferencia.</p>
<p>Escolha entre as duas opções.</p>
<h3><strong>1) Utilizando a base de dados fornecida:</strong></h3>
Importar o arquivo <i>sysescola.sql</i> para o servidor do banco de dados
<br>
<h3><strong>2) Instalacão limpa</strong></h3>
<p>Executar os comando abaixo.</p>
<ul>
	<strong>Commands Artisan:</strong>
	<li><i>php artisan migrate:fresh</i></li>
</ul>
<h1><strong>IMPORTANTE!</strong></h1>
<p>Configurar o arquivo <i>.env</i> com os dados do servidor</p>