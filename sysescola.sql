-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19-Jul-2018 às 15:02
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sysescola`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `alunos`
--

CREATE TABLE `alunos` (
  `id` int(10) UNSIGNED NOT NULL,
  `matricula` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bairro` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cep` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `alunos`
--

INSERT INTO `alunos` (`id`, `matricula`, `nome`, `email`, `endereco`, `bairro`, `cidade`, `uf`, `cep`, `created_at`, `updated_at`) VALUES
(4, '000001', 'Felipe Marques', 'felipefmarques@gmail.com', 'Qms 11 Casa 09', 'Setor de mansões', 'Brasília', 'DF', '73081-665', '2018-07-19 06:47:55', '2018-07-19 08:57:12'),
(5, '000002', 'Luciana Dantas', 'lu@lua.com', 'Quadra 01 Lote 08', 'Acajueiro', 'Brasília', 'DF', '73081-665', '2018-07-19 07:41:42', '2018-07-19 07:41:42'),
(6, '000003', 'Pedro Silva', 'pedro@escola.com', 'Quadra 05', 'Nobre', 'Colina', 'MA', '780909-888', '2018-07-19 07:43:58', '2018-07-19 07:43:58'),
(7, '000004', 'Cláudio Moreira', 'claudio@escola.com', 'Quadra 10 casa 09', 'João de barro', 'taguatinga', 'DF', '780909-888', '2018-07-19 07:45:39', '2018-07-19 08:58:28'),
(9, '000005', 'Bruna Moraes', 'bruna@escola.com', 'Quadra 356 Casa 08', 'Atalaia', 'Aracaju', 'SE', '780909-888', '2018-07-19 08:21:38', '2018-07-19 08:21:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplinas`
--

CREATE TABLE `disciplinas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `disciplinas`
--

INSERT INTO `disciplinas` (`id`, `nome`, `created_at`, `updated_at`) VALUES
(1, 'Matemática', '2018-07-19 13:09:30', '2018-07-19 13:09:30'),
(2, 'Português', '2018-07-19 13:11:30', '2018-07-19 13:16:14'),
(5, 'Física', '2018-07-19 14:42:38', '2018-07-19 14:42:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_07_18_234650_create_alunos_table', 1),
(2, '2018_07_19_093627_create_discplinas_table', 2),
(3, '2018_07_19_093627_create_disciplinas_table', 3),
(4, '2018_07_19_100808_create_disciplinas_table', 4),
(5, '2018_07_19_110824_create_notas_table', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `notas`
--

CREATE TABLE `notas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nota` double(8,2) NOT NULL,
  `id_aluno` int(11) NOT NULL,
  `id_disciplina` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `notas`
--

INSERT INTO `notas` (`id`, `nota`, `id_aluno`, `id_disciplina`, `created_at`, `updated_at`) VALUES
(5, 9.50, 4, 5, '2018-07-19 14:43:05', '2018-07-19 14:43:05'),
(7, 8.00, 4, 5, '2018-07-19 15:08:28', '2018-07-19 15:08:28'),
(8, 6.00, 4, 5, '2018-07-19 15:09:12', '2018-07-19 15:09:12'),
(9, 7.00, 4, 1, '2018-07-19 15:10:02', '2018-07-19 15:10:02'),
(10, 8.00, 4, 1, '2018-07-19 15:10:13', '2018-07-19 15:10:13'),
(11, 9.00, 4, 1, '2018-07-19 15:10:41', '2018-07-19 15:10:41'),
(12, 10.00, 4, 2, '2018-07-19 15:10:58', '2018-07-19 15:10:58'),
(13, 8.00, 4, 2, '2018-07-19 15:11:06', '2018-07-19 15:11:06'),
(14, 5.00, 4, 2, '2018-07-19 15:11:14', '2018-07-19 15:11:14'),
(15, 6.00, 5, 1, '2018-07-19 15:11:29', '2018-07-19 15:11:29'),
(16, 3.00, 5, 1, '2018-07-19 15:11:41', '2018-07-19 15:11:41'),
(17, 8.00, 5, 1, '2018-07-19 15:11:52', '2018-07-19 15:11:52'),
(18, 8.00, 5, 2, '2018-07-19 15:12:31', '2018-07-19 15:12:31'),
(19, 9.00, 5, 2, '2018-07-19 15:12:54', '2018-07-19 15:12:54'),
(20, 9.00, 5, 2, '2018-07-19 15:13:07', '2018-07-19 15:13:07'),
(21, 5.00, 5, 5, '2018-07-19 15:13:32', '2018-07-19 15:13:32'),
(22, 6.00, 5, 5, '2018-07-19 15:13:40', '2018-07-19 15:13:40'),
(23, 5.00, 5, 5, '2018-07-19 15:13:50', '2018-07-19 15:13:50'),
(24, 6.00, 6, 1, '2018-07-19 15:14:20', '2018-07-19 15:14:20'),
(25, 9.00, 6, 1, '2018-07-19 15:14:41', '2018-07-19 15:14:41'),
(26, 8.00, 6, 1, '2018-07-19 15:14:58', '2018-07-19 15:14:58'),
(27, 5.00, 6, 2, '2018-07-19 15:15:06', '2018-07-19 15:15:06'),
(28, 6.00, 6, 2, '2018-07-19 15:15:13', '2018-07-19 15:15:13'),
(31, 4.00, 6, 2, '2018-07-19 15:15:48', '2018-07-19 15:15:48'),
(32, 8.00, 6, 5, '2018-07-19 15:15:56', '2018-07-19 15:15:56'),
(33, 4.00, 6, 5, '2018-07-19 15:16:05', '2018-07-19 15:16:05'),
(34, 6.00, 6, 5, '2018-07-19 15:18:16', '2018-07-19 15:18:16'),
(35, 5.00, 7, 1, '2018-07-19 15:18:30', '2018-07-19 15:18:30'),
(36, 7.00, 7, 1, '2018-07-19 15:18:45', '2018-07-19 15:18:45'),
(37, 9.00, 7, 1, '2018-07-19 15:18:53', '2018-07-19 15:18:53'),
(38, 7.00, 7, 2, '2018-07-19 15:18:59', '2018-07-19 15:18:59'),
(39, 9.00, 7, 2, '2018-07-19 15:19:07', '2018-07-19 15:19:07'),
(40, 7.00, 7, 2, '2018-07-19 15:19:20', '2018-07-19 15:19:20'),
(41, 4.00, 7, 5, '2018-07-19 15:19:33', '2018-07-19 15:19:33'),
(42, 8.00, 7, 5, '2018-07-19 15:19:43', '2018-07-19 15:19:43'),
(43, 1.00, 7, 5, '2018-07-19 15:19:51', '2018-07-19 15:19:51'),
(44, 4.00, 9, 1, '2018-07-19 15:20:02', '2018-07-19 15:20:02'),
(45, 6.00, 9, 1, '2018-07-19 15:20:10', '2018-07-19 15:20:10'),
(46, 6.50, 9, 1, '2018-07-19 15:20:21', '2018-07-19 15:20:21'),
(47, 9.00, 9, 2, '2018-07-19 15:20:34', '2018-07-19 15:20:34'),
(48, 8.00, 9, 2, '2018-07-19 15:20:52', '2018-07-19 15:20:52'),
(49, 10.00, 9, 2, '2018-07-19 15:21:04', '2018-07-19 15:21:04'),
(50, 5.00, 9, 5, '2018-07-19 15:21:20', '2018-07-19 15:21:20'),
(51, 7.00, 9, 5, '2018-07-19 15:21:27', '2018-07-19 15:21:27'),
(53, 8.00, 9, 5, '2018-07-19 15:22:34', '2018-07-19 15:22:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alunos`
--
ALTER TABLE `alunos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disciplinas`
--
ALTER TABLE `disciplinas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alunos`
--
ALTER TABLE `alunos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `disciplinas`
--
ALTER TABLE `disciplinas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `notas`
--
ALTER TABLE `notas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
