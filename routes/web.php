<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('home');

})->name('home');



//alunos
Route::get('/alunos/editar/{id}','AlunosController@edit');
Route::get('/alunos/apagar/{id}','AlunosController@destroy');

Route::post('/alunos','AlunosController@store');
Route::post('/alunos/{id}','AlunosController@update');

Route::resource('/alunos','AlunosController')->names([
    'create' => 'adicionarAluno',
    'index' => 'alunos'
]);


//disciplinas
Route::get('/disciplinas/editar/{id}','DisciplinasController@edit');
Route::get('/disciplinas/apagar/{id}','DisciplinasController@destroy');

Route::post('/disciplinas','DisciplinasController@store');
Route::post('/disciplinas/{id}','DisciplinasController@update');

Route::resource('/disciplinas','DisciplinasController')->names([
    'create' => 'adicionarDisciplina',
    'index' => 'disciplinas'
]);


//notas
Route::get('/notas/editar/{id}','NotasController@edit');
Route::get('/notas/apagar/{id}','NotasController@destroy');

Route::post('/notas','NotasController@store');
Route::post('/notas/{id}','NotasController@update');

Route::resource('/notas','NotasController')->names([
    'create' => 'adicionarNota',
    'index' => 'notas'
]);

Route::get('/relatorio','NotasController@relatorio')->name('relatorio');










