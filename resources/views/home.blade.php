@include('header')
<section class="grid-x grid-padding-x ">
    <div class="cell large-10 large-offset-2">
        <h3>Respostas:</h3>
        <p><strong>1</strong> – De acordo com as novas versões do Laravel é possível fazer referência a uma classe da seguinte maneira:</p>
        <ol>
            <li>$this->hasMany('App\Usuario');</li>
            <li>$this->hasMany(Usuario::class);</li>
            <li>$this->hasMany(App\Usuario);</li>
        </ol>
        <ol>
            <p>Marque alternativa correta:</p>
            <li><strong>( x ) a e b estão corretas;</strong></li>
            <li>(  &nbsp;&nbsp; ) b e c estão corretas;</li>
            <li>(  &nbsp;&nbsp; ) c e a estão corretas;</li>
        </ol>      
        <p><strong>2</strong> – Em relação a construção de rotas, monte a rota apresentada na figura abaixo:</p>
        <img src="images/rota.png"/>
        <h4>Resposta:</h4>
        <p>Existem algumas formas de fazer uma rota como essa, a primeira uma rota direta e a segunda uma rota agrupada. Abaixo estão ambos os exemplos:</p>
        <br/>
        <h4>Formas diretas</h4>
        <h5>Route::get('/livros/primeiros-passos-docker', function () {});</h5>
        <h5>Route::get('/livros/{titulo}', function ($titulo) {});</h5>
        <br/>
        <h4>Forma agrupada</h4>
        <h5>Route::prefix('livros')->group(function () {
            <ul>
                <li>Route::get('/', function () {});</li>
                <li>Route::get('/primeiros-passos-docker/', function () {});</li>
                <li>Route::get('/tudo-sobre-laravel/', function () {});</li>
            </ul>
        });

        </h5>
    </div>
</section>
@include('footer')