@include('header')

<section class="grid-x grid-padding-x">
    <div class="cell large-10 large-offset-2">
        <h2>Adicionar disciplina</h2>
        <br/>
        <form action='/disciplinas' method="POST">

          <div class="grid-container">
                @csrf
            <div class="grid-x grid-padding-x">
              <div class="large-6 cell">
                <label>Nome
                  <input type="text" name="nome"/>
                </label>  

            <div class="grid-x grid-padding-x">
              <div class="large-8 cell">
                <input type="submit" class="button" value="Adicionar disciplina">
                <a href="{{ route('disciplinas')}}" class="button">Cancelar</a>
              </div>
            </div>


          </div>
          </div>
        </form>
        <div class="grid-container full">
  <div class="grid-x grid-margin-x">
          @if( $errors->any() )

                @foreach($errors->all() as $error)
                   <div class="alert-box">
          
                      {{'*'. $error}}
                      
                    </div> 
                       @endforeach   
   
    

     
        @endif

  </div>
</div> 
  
</section>
@include('footer')