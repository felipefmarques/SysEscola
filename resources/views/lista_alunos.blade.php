@include('header')

<section class="grid-x grid-padding-x grid-margin-x">
    <div class="cell large-8 large-offset-2">
        <h2 class="float-left">Alunos </h2>
        <a href="{{route('adicionarAluno')}}" class="button float-right">Adicionar Aluno</a>
        <table class="large-10">
        	<thead>
        		<tr>
	        		<th>Matricula</th>
	        		<th>Nome</th>
	        		<th>E-mail</th>
                    <th>Endereço</th>
	        		<th></th>
        		</tr>
        	</thead>
        	<tbody>
        		@foreach($alunos as $a)
        		<tr>
        			<td>{{ $a->matricula }}</td>
        			<td>{{ $a->nome }}</td>
        			<td>{{ $a->email }}</td>
                    <td>{{ $a->endereco . ' - ' . $a->bairro . ', ' . $a->cidade . ' - ' . $a->uf . ', ' . $a->cep }}</td>
        			<td>
                        <a href="/alunos/editar/{{ $a->id }}"><i class="fas fa-edit"></i></a>         
                        <a href="/alunos/apagar/{{ $a->id }}"><i class="far fa-trash-alt"></i></i></a>         
                    </td>
        		</tr>
        		@endforeach  
        	</tbody>
        </table>
       
    </div>
</section>
@include('footer')