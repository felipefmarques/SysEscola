@include('header')

<section class="grid-x grid-padding-x">
    <div class="cell large-10 large-offset-2">
        <h2>Adicionar nota</h2>
        <br/>
        <form action='/notas' method="POST">
          @csrf
          <div class="grid-container">
            <div class="grid-x grid-padding-x">            
              <div class="large-2 cell">
                <label>Disciplina
                  <select name="id_disciplina">
                    <option value="">Selecione</option>
                    @foreach($disciplinas as $d)
                    <option value="{{$d->id}}">{{$d->nome}}</option>
                    @endforeach
                  </select>
                </label>
              </div>
             <div class="large-2 cell">
                <label>Aluno
                  <select name="id_aluno">
                    <option value="">Selecione</option>
                    @foreach($alunos as $a)
                    <option value="{{$a->id}}">{{$a->nome}}</option>
                    @endforeach
                  </select>
                </label>
              </div>
               <div class="large-2 cell">                
                <label>Valor da nota
                  <input type="number" name="nota" min="0" max="10"/>
                </label>
              </div>            
            </div>

            <div class="grid-x grid-padding-x">
              <div class="large-5 cell">
                <input type="submit" class="button" value="Adicionar nota">
                <a href="{{ route('notas')}}" class="button">Cancelar</a>
              </div>
            </div>


          </div>
          </div>
        </form>
        <div class="grid-container full">
  <div class="grid-x grid-margin-x">
          @if( $errors->any() )

                @foreach($errors->all() as $error)
                   <div class="alert-box">
          
                      {{'*'. $error}}
                      
                    </div> 
                       @endforeach   
   
    

     
        @endif

  </div>
</div> 
  
</section>
@include('footer')