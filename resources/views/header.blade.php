<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Sagres - SysEscola</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ url('/') }}/css/app.css">
        <link rel="stylesheet" href="{{ url('/') }}/css/foundation.css">        
        <link rel="stylesheet" href="{{ url('/') }}/css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


    </head>
    <body>
        <header class="row expanded">
            <h4 class="float-left"><a href="{{ route('home') }}">SysEscola</a></h4>
            <ul class="menu align-right">
              <li><a href="{{ route('alunos') }}">Alunos</a></li>
              <li><a href="{{ route('disciplinas') }}">Disciplinas</a></li>
              <li><a href="{{ route('notas') }}">Notas</a></li>
              <li><a href="{{ route('relatorio') }}">Relatório</a></li>
            </ul>
        </header>