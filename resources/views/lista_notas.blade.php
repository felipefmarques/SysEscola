@include('header')

<section class="grid-x grid-padding-x grid-margin-x">
    <div class="cell large-8 large-offset-2">
        <h2 class="float-left">Notas </h2>
        <a href="{{route('adicionarNota')}}" class="button float-right">Adicionar nota</a>
        <table class="large-10">
        	<thead>
        		<tr>
                    <th>Aluno</th>
	        		<th>Nota</th>
	        		<th></th>
        		</tr>
        	</thead>
        	<tbody>
                {{$dname = ''}}
        		@foreach($notas as $n)
                @if($dname != $n->a_nome)
                <tr>
                    <td class="aluno-linha"><strong>{{$dname = $n->a_nome }}</strong</td>
                    <td class="aluno-linha"></td>
                    <td class="aluno-linha"></td>                      
                </tr>
                @endif
        		<tr>
                    <td>{{ $n->d_nome }}</td>
        			<td>{{ $n->nota }}</td>
        			<td class="txt-align-right">
                        <a href="/notas/editar/{{ $n->id }}"><i class="fas fa-edit"></i></a>         
                        <a href="/notas/apagar/{{ $n->id }}"><i class="far fa-trash-alt"></i></i></a>         
                    </td>
        		</tr>
        		@endforeach  
        	</tbody>
        </table>
       
    </div>
</section>
@include('footer')