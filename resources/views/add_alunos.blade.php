@include('header')

<section class="grid-x grid-padding-x">
    <div class="cell large-10 large-offset-2">
        <h2>Adicionar aluno</h2>
        <br/>
        <form action='/alunos' method="POST">

          <div class="grid-container">
            <div class="grid-x grid-padding-x">
              <div class="large-4 cell">
                @csrf
                <label>Matrícula do aluno
                  <input type="text" name="matricula"/>
                </label>
              </div>
            </div>

            <div class="grid-x grid-padding-x">
              <div class="large-6 cell">
                <label>Nome
                  <input type="text" name="nome"/>
                </label>
              </div>
              <div class="large-4 cell">
                <label>E-mail
                  <input type="email" name="email"/>
                </label>
              </div>
            </div>

            <div class="grid-x grid-padding-x">
              <div class="large-5 cell">
                <label>Endereço
                  <input type="text" name="endereco"/>
                </label>
              </div>
              <div class="large-5 cell">
                <label>Bairro
                  <input type="text" name="bairro"/>
                </label>
              </div>
            </div>

            <div class="grid-x grid-padding-x">
              <div class="large-4 cell">
                <label>Cidade
                  <input type="text" name="cidade"/>
                </label>
              </div>
              <div class="large-3 cell">
                <label>Unidade da Federação
                  <select name="uf">
                    <option value="">Selecione</option>
                    <option value="AC">Acre</option>
                    <option value="AL">Alagoas</option>
                    <option value="AP">Amapá</option>
                    <option value="AM">Amazonas</option>
                    <option value="BA">Bahia</option>
                    <option value="CE">Ceará</option>
                    <option value="DF">Distrito Federal</option>
                    <option value="ES">Espirito Santo</option>
                    <option value="GO">Goiás</option>
                    <option value="MA">Maranhão</option>
                    <option value="MS">Mato Grosso do Sul</option>
                    <option value="MT">Mato Grosso</option>
                    <option value="MG">Minas Gerais</option>
                    <option value="PA">Pará</option>
                    <option value="PB">Paraíba</option>
                    <option value="PR">Paraná</option>
                    <option value="PE">Pernambuco</option>
                    <option value="PI">Piauí</option>
                    <option value="RJ">Rio de Janeiro</option>
                    <option value="RN">Rio Grande do Norte</option>
                    <option value="RS">Rio Grande do Sul</option>
                    <option value="RO">Rondônia</option>
                    <option value="RR">Roraima</option>
                    <option value="SC">Santa Catarina</option>
                    <option value="SP">São Paulo</option>
                    <option value="SE">Sergipe</option>
                    <option value="TO">Tocantins</option>
                  </select>
                </label>
              </div>
              <div class="large-3 cell">
                <label>CEP
                  <input type="text" name="cep"/>
                </label>
              </div>              
            </div>

            <div class="grid-x grid-padding-x">
              <div class="large-5 cell">
                <input type="submit" class="button" value="Adicionar aluno">
                <a href="{{ route('alunos')}}" class="button">Cancelar</a>
              </div>
            </div>


          </div>
          </div>
        </form>
        <div class="grid-container full">
  <div class="grid-x grid-margin-x">
          @if( $errors->any() )

                @foreach($errors->all() as $error)
                   <div class="alert-box">
          
                      {{'*'. $error}}
                      
                    </div> 
                       @endforeach   
   
    

     
        @endif

  </div>
</div> 
  
</section>
@include('footer')