@include('header')

<section class="grid-x grid-padding-x grid-margin-x">
    <div class="cell large-8 large-offset-2">
        <h2>Relatório</h2>

        @foreach($medias as $d => $a)
        <br/>
        <h5>{{$d}}</h5>
        <table class="large-10">
            <thead>
                <tr>
                    <th>Aluno</th>
                    <th>Média</th>
                </tr>
            </thead>
         @foreach($a as $nome => $obj)
        
        	
        	<tbody>
        		
        		<tr>
                    <td>{{$nome}}</td>
                    <td>{{round($obj['media'],2)}}</td>
        		</tr>
        		 
        	</tbody>
        
        @endforeach 
        </table>
@endforeach 


       
    </div>
</section>
@include('footer')