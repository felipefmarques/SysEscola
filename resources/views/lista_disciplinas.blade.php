@include('header')

<section class="grid-x grid-padding-x grid-margin-x">
    <div class="cell large-8 large-offset-2">
        <h2 class="float-left">Disciplinas </h2>
        <a href="{{route('adicionarDisciplina')}}" class="button float-right">Adicionar disciplina</a>
        <table class="large-10">
        	<thead>
        		<tr>
	        		<th>Nome</th>
	        		<th></th>
        		</tr>
        	</thead>
        	<tbody>
        		@foreach($disciplinas as $d)
        		<tr>
        			<td>{{ $d->nome }}</td>
        			<td class="txt-align-right">
                        <a href="/disciplinas/editar/{{ $d->id }}"><i class="fas fa-edit"></i></a>         
                        <a href="/disciplinas/apagar/{{ $d->id }}"><i class="far fa-trash-alt"></i></i></a>         
                    </td>
        		</tr>
        		@endforeach  
        	</tbody>
        </table>
       
    </div>
</section>
@include('footer')